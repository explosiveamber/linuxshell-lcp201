/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lcp201;

/**
 *
 * @author explosiveamber
 */
public class EsempiFor {
    
    
    public static void indiceNon1(){
    
    
        System.out.println("3.Indice non corrispondente a 1");
        for(int i= 0; i <= 1000000; i = i+100){
        
            System.out.format("Valore attuale di i: %d\n", i);
        }
    
    }
    
    public static void indiceCasuale(){

        
      System.out.println("4.Indice casuale");
      
      String alfabeto = "abcdefghijklmnopqrstuvwyz";
      
      for(int i=0; i <= 100; i++){
          
          /* algoritmo di generazione dell' indice. 
             1. moltiplica Math.random a lunghezza stringa
             2. CAST double -> int  (ATTENZIONE: CAST HA PRECEDENZA SU PRODOTTO)
                PORRE IL PRODOTTO TRA PARENTESI, E DOPO CASTARE IL TUTTO!!!
          */
     
          int indice = (int)(Math.random() * alfabeto.length());
          
          // con il metodo charAt della classe String, rimediamoci il carattere 
          
          char c = alfabeto.charAt(indice);
      
          System.out.format("%d : %s\n", indice, c);
      
      }
      

}
    
    public static void copiaNumeriPari(){
        System.out.println("1.Copia numeri pari");
    
        // 
        int[] primo = new int[]{12,11,5,77,4,90,33,18,44,26};
        // 
        int[] secondo = new int[10];
    
        for(int i=0; i<10; i++){
            
            // Se il numero è divisibile per 2, copiarlo in pos.corrispondente
            if(primo[i] % 2 == 0){
            
                secondo[i] = primo[i];
            }
            else{
            // Altrimenti assegna alla pos.corrispondente  il valore 0
                secondo[i] = 0;
            }    
        }
        for(int i=0; i<10; i++){
        
            System.out.println(secondo[i]);
            System.out.println("------------------");
        }
        System.out.println("------------------");
        
    }
    
    public static void cicliAnnidati(){
    
        int a = 0;
        int b = 0;
        float c = 0.0f;
        
        
        
        System.out.println("2.Cicli annidati");
        
        for(int i = 0; i < 20; i++){
        
            for(int j = 0; j < 20; j++){
                
                for(int k = 0; k <20; k++){
                    
                }
                
            }
           a++;
           b = b - 2;
           c = c + 3.14f;
        }
        
        System.out.println(a);
        System.out.println(b);
        System.out.format("Valore di c: %.1f%n", c);
        System.out.println("------------------");
    }
    
    public static void cicloFrequenzaCasuale(){
    
        System.out.println("5.Ciclo a frequenza casuale");
        
        // Nota : un algoritmo migliore è implementato in Random.randInt()
        int volte = (int)(Math.random() * ((int)(Math.random() * 100)));
        
        System.out.println("Iterazionida compiere : " + volte + "\n");
        while(volte > 0){
        
            System.out.println("Mancano " + volte + " iterazioni.\n");
            volte--;
        }   
    }
    
    public static void main(String[] args){
    
        copiaNumeriPari();
        cicliAnnidati();
        indiceNon1();
        indiceCasuale();
        cicloFrequenzaCasuale();
    }
}

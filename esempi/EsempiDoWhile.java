/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lcp201;

/**
 *
 * @author explosiveamber
 */
public class EsempiDoWhile {
    
    public static void potenza(double num, double pot, int cifre){
    
        double n = num;
        StringBuilder format = new StringBuilder("%.4f");
        format.insert(cifre, 2);
        
        do {
        
            n = n * n;
            pot--;
        
        } while(pot < 1);
        System.out.format(format.toString(),n);
    }
    
    public static void main(String args[]){
    
        potenza(4.0, 2.0, 2);
        potenza(Math.E, 2.0, 2);
        potenza(Math.sqrt(2), 2, 2);
    }
}

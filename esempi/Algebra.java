/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp201;

/**
 */
 /*
 * @author explosiveamber
 */
public class Algebra {
    
    public double diffQuad(double quad_a, double quad_b){
    
        /* in questo caso abbiamo come input due quadrati. 
           Pertanto i due parametri del metodo sarà conveniente denominarli
           quad_a e quad_b, rispettivamente.
        
           Ritornando il risultato si può scegliere di:
           a) ritornare il risultato su una singola riga
           b) 1.creare due variabili di tipo double, somma e diff.
              2.ritornare il loro prodotto. 
        
           L'alternativa è commentata sotto l'istruzione return del metodo.
        */
        
        double a = Math.sqrt(quad_a);
        double b = Math.sqrt(quad_b);
        
        double somma = a + b;
        double diff =  a - b;
        return somma * diff;
        // return (a + b) * (a - b);
    
    }
  
   public double quadBin(double a, double b){
       
       /* Dobbiamo risolvere il quadrato di binomio ricordando che
          il procedimento non è analogo a quelli conosciuti per le potenze.
       Occorre:
       elevare al quadrato a;
       elevare al quadrato b;
       sommare il quadrato di a, il prodotto di a e di b raddoppiato 
       e il quadrato di b.
       
       I parametri (di tipo double) saranno perciò nominati a e b.
       All'interno del metodo creeremo due variabili locali di tipo double
       per ospitare i valori corrispondenti alle due operazioni di elevazione
       al quadrato, rispettivamente, di a e di b.
       L'ultima operazione è abbastanza evidente ed è la trasposizione della
       formula aperta del quadrato di binomio in Java.
       
       */
       double quad_a = Math.pow(a, 2);
       double quad_b = Math.pow(b, 2);
       
       return quad_a + 2 * (a * b) + quad_b;
   
   }
}

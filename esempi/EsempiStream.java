/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esempistream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author explosiveamber
 */
public class EsempiStream {

    /**
     * @param args the command line arguments
     */
    
    public static void scriviUnByte(String percorso, byte b) throws 
            FileNotFoundException, IOException{
    
        /*il metodo lancia (=throws) FileNotFoundException e IoException: 
          quand'è cos+ il try catch è
          inutile perchè ridondante; a gestire l'eccezione sarà il metodo
          chiamante.
        */
        
        /* 1. ISTANZIAZIONE*/
        FileOutputStream stream = new FileOutputStream(new File(percorso));
        
        /* 2. SCRITTURA DEL BYTE*/
        stream.write(b);
        
        /* 3. CHIUSURA DELLO STREAM (IMPORTANTE!!!) */
        stream.close();
    }
    
    public static void leggiUnByte(String percorso) throws
            FileNotFoundException, IOException{
        
        /* Per i throws, vedi commento al metodo precedente. */
    
        int letto = 0;
        /* 1. ISTANZIAZIONE*/
        FileInputStream stream = new FileInputStream(new File(percorso));
        
        /* 2. CONTROLLO DELLA LUNGHEZZA */
        if(stream.available() > 0){
        
        /* 3. LETTURA DEL BYTE */
        letto = stream.read();
        
        }
        /* 4. CHIUSURA DELLO STREAM (IMPORTANTE!!!)*/
        stream.close();
        
        /* Verifica del risultato */
        System.out.println((char)letto);
    }
    
    public static void scriviByte(String percorso, byte[] vettore) throws
            FileNotFoundException, IOException{
    
        /* 1. ISTANZIAZIONE DELLO STREAM*/
        /* Sorpresa!*/
        OutputStream out = new FileOutputStream(new File(percorso)); //[1]
        
        /* 2. SCRITTURA DEI DATI*/
        out.write(vettore);
        
        /* 4. CHIUSURA DELLO STREAM (IMPORTANTE!!!)*/
        out.close();
    }
    /* [1] Si può dichiarare un oggetto della superclasse
        e instanziarne uno della corrispondente sottoclasse
       (una delle conseguenze dell'ereditarietà)
    */
    
    public static void leggiByte(String percorso, int quanti) throws FileNotFoundException, 
            IOException{
        
        /* 1. ISTANZIAZIONE DELLO STREAM*/
        InputStream in = new FileInputStream(new File(percorso));
        
        /* 2. CREAZIONE DEL VETTORE DI APPOGGIO*/
        byte[] buffer = new byte[quanti];
        
        /* 3. CONTROLLO LUNGHEZZA */
        while(in.available() != 0){
        
        /* 4. LETTURA DEI DATI*/
        
        
        in.read(buffer);
            
        /* 5. CHIUSURA DELLO STREAM (IMPORTANTE!!!)*/
        /*.. Ma in questo caso si farà come ultima operazione perchè dobbiamo
        ancora accedere all'array, e dobbiamo farlo a stream ancora aperto. 
        (È un metodo con side effect.) */
       
        }
        
        /* 6. STAMPA DEI BYTES LETTI */
        int conto = 0;
        System.out.println("Lunghezza del buffer:\n" + buffer.length + " bytes\nContenuti\n");
        while(conto < buffer.length){ 
            System.out.print((char)buffer[conto]);
            conto++;
                }
        /* Quindi si chiude lo stream. */
        in.close();
       
    }
    
    public static void main(String[] args) {
        try {
            
            // TODO
            
            //leggiUnByte(""); //arg0: percorso del file
            
            leggiByte("", 0);//arg0 : percorso del file   arg1: numero di byte
            
            //scriviByte("", new byte[]{});//arg0: percorso del file   arg1: vettore di byte
            
        } catch (IOException ex) {
            
            System.err.println("ERRORE");
            ex.printStackTrace();
            System.exit(-1);
        }
        
    }
    
}

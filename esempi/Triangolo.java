/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 */
 /*
 * @author explosiveamber
 */

package lcp202;

 public class Triangolo {
    
        private double ab, ac, bc;
        
        public Triangolo(double ab, double ac, double bc){
        
            this.ab = ab;
            this.ac = ac;
            this.bc = bc;
        }
        
        public int hashCode(){
        
            int risultato = 17;
            risultato *= ab;
            risultato *= bc;
            risultato *= bc;
            return 31 * risultato;
        }
        
        public boolean equals(Object o){
        
            if(!(o instanceof Triangolo)) return false;
            Triangolo other = (Triangolo) o;
            return (this.ab == other.ab && 
                    this.bc == other.bc &&
                    this.ac == other.ac);
        }
        
        public String toString(){
        
            return  "ab: " + this.ab + 
                    "bc: " + this.bc +
                    "ac: " + this.ac; 
        }
    }
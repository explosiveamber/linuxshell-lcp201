/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp201;

/**
 */
 /*
 * @author explosiveamber
 */
public class Casting {
    
    public static void main(String[] args){
    
    // ####################################
        byte b = 125;
        char c = (char) b;
        
        System.out.println("Byte: " + b );
        System.out.println("Byte -> Carattere : " + c);
        
    // ####################################
        
        int i = 1000;
        float f = (float) i;
        
        System.out.println("Intero: " + i);
        System.out.println("Intero -> Float: " + f);
        
    // ####################################
        
        double d = 3.1415;
        int i2 = (int) d;
        
        System.out.println("Double: " + d);
        System.out.println("Double -> Intero:" + i2);
    }

}

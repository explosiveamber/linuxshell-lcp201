/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp202;

import java.util.Arrays;

/**
 */
 /*
 * @author explosiveamber
 */
public class EsempiArrayAvanzati {
    
    
    public static double[] calcolaPunti(double[] arrX, double[] arrY){
    /**  Moltiplica le coordinate x e y di una serie di punti ritornando la 
     serie delle loro posizioni sul piano cartesiano. **/
        
        double[] punti = new double[arrX.length];
        
        
        if(arrX.length == arrY.length){  // Se i due insiemi di punti hanno la 
            // stessa cardinalità procediamo oppure restituiamo una serie vuota.
        
        
        for(int i = 0; i < arrX.length; i++){
          punti[i] = arrX[i] * arrY[i]; 
          }
        }
        
        return punti;
    }
    
    
    public static double prodScalare(double[] vA, double[] vB){
        /** Applica la formula del prodotto scalare su due vettori **/
    
        double risultato = 0.0;
        
        if(vA.length == vB.length){ // se la lunghezza dei due vettori coincide
        
            double partA = 1.0;
            double partB = 1.0;
            
            for(int i = 0; i < vA.length; i++){
            // moltiplica elementi dei due vettori accumulandoli
                partA *= vA[i];
                partB *= vB[i];
                
            }
            
            risultato = partA + partB; // ne esegue la somma
            
        }
        
        return risultato; // e ritorna il risultato
    }
    
    public static void interpolazione(double[] vA, double[] vB, double[] vC){
    
        if((vA.length == vB.length) && (vB.length == vC.length)){}
 
        double partA = 1.0;
        double partB = 1.0;
        
        for(int i = 0; i < vA.length; i++){
        
            partA *= vA[i] + vC[i];
            partB *= vB[i] + vC[i];
            
            System.out.println(partA + " " + partB + " (" + vC[i] + ") ");
        }
        
    }
    
    public static void main(String[] args){
    
      double[] x = new double[]{3.5, 7.2, 1.4}; // insieme delle x
      double[] y = new double[]{8.8, 9.1, 15.4};// insieme delle y
      double[] z = new double[]{0.1, 0.3, 0.1}; // vet. di interpolazione
      String p = Arrays.toString(EsempiArrayAvanzati.calcolaPunti(x, y));
      
      System.err.println(p);
      
      double scal = EsempiArrayAvanzati.prodScalare(x, y);
      System.out.println(scal);
      EsempiArrayAvanzati.interpolazione(x, y, z);
    }

}

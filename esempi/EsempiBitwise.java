/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp202;

import java.util.Arrays;

/**
 */
 /*
 * @author explosiveamber
 */
public class EsempiBitwise {
    
    public static String[] converti(int[] interi){
        /** Converte un array di interi nella rispettiva rappresentazione base2
         **/
    
        String[] output = new String[interi.length];
        for(int i = 0; i < interi.length; i++){
           output[i] = Integer.toBinaryString(interi[i]);
        }
        return output;
    }
    
    
    public static int[] bitwiseAnd(int[] v1, int[] v2){
        /** Esegue un operazione bitwise su un array di interi **/
    
        int[] v3 = new int[v1.length];
        
        if(v1.length == v2.length){
        
        
        for(int i = 0; i < v1.length; i++){
        
            v3[i] = v1[i] & v2[i];
        }
        }
        return v3;
    
    }
    
    public static void main(String[] args){
    
        int[] a = new int[]{0b0011, 0b0011, 0b0011};
        int[] b = new int[]{0b1111, 0b1111, 0b1111};
        System.out.println(
                //Arrays.toString(bitwiseAnd(a,b)));
                Arrays.toString(EsempiBitwise.converti(
                        EsempiBitwise.bitwiseAnd(a, b))
                )
        );
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lcp201;

import java.util.Random;

/**
 *
 * @author explosiveamber
 */
public class EsempiBreakContinue {
    
    public static int somma1(){
    
    int result = 0;
    int i = 100;
    for(int c = 0; c < i; i++){

        if(result >= 50){
        
            break;
        }
        result ++;
    }
  
    return result;
}
    
   public static int somma2(int arg, int volte){
    
    int result = 0;
    int i = 100;
    for(int c = 0; c < i; i++){

        if(result >= volte){
        
            break;
        }
        result += arg;
    }
  
    return result;
}
   
   public static int calcolaInIntervallo(int q){
   
       int ris = 0;
       int quantita = q;
       
       while((quantita < 100) && (quantita > 50)){
   
          if(q == 75) break; 
          quantita++;
          
     }
       return quantita;
   }
   
   public static int trovaLettera(char t, String parola){
   
       int volte = 0;
       char trovata =  '\u0000'; // valore predefinito per variabile di tipo char
       // per questo tipo di iterazione va bene un ciclo for, ma non sempre
       for(int i = 0; i < parola.length(); i++){
       
           trovata = parola.charAt(i);
           if(trovata == t) 
               volte += 1;
               continue;
       }
       return volte; 
   }
   
   public static boolean trovaCarattere(String daTestare){
   
       boolean test = false;
       char b = '\u004D'; //M
       //char b = '\u0020'; //SPACE_SEPARATOR
       char trovato = '\u0000'; // valore default per tipo char
       for(int i = 0; i < daTestare.length(); i++){
       
           trovato = daTestare.charAt(i);
           if(trovato == b) 
               test = true;
               break; 
       }
       return test;
   }
   
   public static String calciDiRigore(String squadra1, String squadra2){
   
       String vince = null;
       Random r = new Random();
       int calci = 3;
       int conta = 0;
       boolean goal1 = false;
       boolean goal2 = false;
       while(conta < calci){
       
           goal1 = r.nextBoolean();
           goal2 = r.nextBoolean();
           if(goal1){
           
               conta += 1;
               vince = squadra1;
           }
           else if(goal2){
           
               conta += 1;
               vince = squadra2;
           }
           else{
                // condizione si può omettere
           }
       }
       return vince;
   }
   
   public static int trovaInterruzione(String test){
   
       int volte = 0;
       int conta = 0;
       while(conta < test.length()){
           // Verifica se il carattere in esame è uno spazio (\r o \n o \t o \x24b o \u0012)
           boolean trovato = Character.isWhitespace(test.charAt(conta));
           if(trovato){
               volte += 1;
           }
           // In questo caso utilizzare istruzioni di controllo flusso non è necessario
           conta++;
       }
       
       return volte;
   }
   
   public static void main(String[] args){
       int x1 = somma1();
       int x2 = somma2(5, 50);
       System.out.println(x1);
       //System.out.println(x2);
       System.out.println("Invoca calcolaIntervallo(63):" + calcolaInIntervallo(63));
       System.out.println("Invoca calcolaIntervallo(63):" + calcolaInIntervallo(75));
       System.out.println("Invoca trovaLettera('t', \"Torino\"):" + trovaLettera('t', "Torino"));
       System.out.println("Invoca trovaLettera('t', \"Trento\"): t è presente " + trovaLettera('t', "Trento") + " volte");
       System.out.println("Invoca trovaLettera('t', \"Trento\"): t è presente " + trovaLettera('t', "Toronto") + " volte");
       System.out.println(trovaCarattere("Ma no\nEccolo"));
       System.out.println(trovaCarattere("ma"));
       System.out.println("Vince:" + calciDiRigore("Roma", "Lazio"));
       System.out.println("Gli spazi presenti sono " + trovaInterruzione("Non so se vengo")+".");
   }
}
/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import java.text.DecimalFormat;

/**
 */
 /*
 * @author explosiveamber
 */
public class UsaArea {
    
    // calcolo area triangolo
    
    public static void main(String[] args){
        
        float altezza = 20.7f;
        float base = 11.4f;
    
        float risultato = Area.triangolo(altezza, base);
        
        DecimalFormat df = new DecimalFormat("###.##");
        System.out.println("L'AREA DEL TRIANGOLO È: " + df.format(risultato));
        System.exit(0);
    }
    
}

/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp202;

/**
 */
 /*
 * @author explosiveamber
 */
public class EsempioClasseIstanziata {
    
    
    public static void main(String[] args){
    
        EsempioClasseIstanziata eci = new EsempioClasseIstanziata();
        Triangolo t1 = new Triangolo(200.0, 200.0, 200.0);
        Triangolo t2 = new Triangolo(200.0, 200.0, 200.0);
        Triangolo t3 = new Triangolo(201.0, 200.0, 200.0);
        if(t1 == t2) System.out.println("Siamo identici");
        if(t1.equals(t2)) System.out.println("Siamo uguali");
        if(!t1.equals(t3)) System.out.println("Siamo diversi"); // [1]
        if(t1 == t1) System.out.println("Siamo identici");
        
    }

    /* [1] I concetti di 'uguale' e 'diverso' sono opposti.
           Così come lo è il loro valore di verità.
           Nota che se si si confrontano due oggetti diversi 
           usando il metodo equals, esso resituisce un valore di verità FALSO.
    
           Quando questo accade:
           l'uguaglianza dei due oggetti ha valore di verità FALSO, 
           la diversità dei due oggetti ha valore di verità VERO.
           Dato quanto sopra si usa, per testare la diversità logica, usando 
           l'operatore boolean not in precedenza sull'espressione
    */
}

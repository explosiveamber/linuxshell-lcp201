/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp202;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 */
 /*
 * @author explosiveamber
 */
public class EsempioLock {
    
    
    private static Object o1 = new Object();
    private static Object o2 = new Object();
    
    public static void main(String[] args){
    
        synchronized(o1){
        
            try {
                o1.wait(5000);
                System.out.println("Oggetto" + o1 + ":Attesa terminata (5 sec)");
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
        synchronized(o2){
        
            try {
                o2.wait(10000);
                System.out.println("Oggetto" + o1 + ":Attesa terminata (10 sec)");
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

}

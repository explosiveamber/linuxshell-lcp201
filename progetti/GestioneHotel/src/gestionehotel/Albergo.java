/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionehotel;

/**
 */
 /*
 * @author explosiveamber
 */

import java.util.LinkedList;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Albergo extends Frame {
    
    private final String PIANO1 = "piano1.txt";
    private final String PIANO2 = "piano2.txt";
    private final String PIANO3 = "pianot3.txt";
    private final String OCCUPATE = "occupate.txt";
    
    private LinkedList<Stanza> piano1, piano2, piano3, occupate;
    private Button bCarica, bCerca, bPrenota, bSalva;
    
    class Chiusura extends WindowAdapter {
    
        LinkedList<Stanza> piano1, piano2, piano3, occupate;
        private final String PIANO1 = "piano1.txt";
        private final String PIANO2 = "piano2.txt";
        private final String PIANO3 = "pianot3.txt";
        private final String OCCUPATE = "occupate.txt";
        
        public Chiusura(LinkedList<Stanza> piano1, 
                        LinkedList<Stanza> piano2,
                        LinkedList<Stanza> piano3,
                        LinkedList<Stanza> occupate){
        
            this.piano1 = piano1;
            this.piano2 = piano2;
            this.piano3 = piano3;
            this.occupate = occupate;
        }
        
        @Override
        public void windowClosing(WindowEvent e){
        
            System.out.println("Salvataggio dati..Attendere per favore.");
            
            int[] sp1 = new int[piano1.size()];
            int[] sp2 = new int[piano2.size()];
            int[] sp3 = new int[piano3.size()];
            int[] so = new int[occupate.size()];
            
            for(int i = 0; i < piano1.size(); i++) {
            
                sp1[i] = piano1.get(i).getNumero();
                sp2[i] = piano2.get(i).getNumero();
                sp3[i] = piano3.get(i).getNumero();
                so[i] =  occupate.get(i).getNumero();
            }
            
            Dati.salva(sp1, PIANO1);
            Dati.salva(sp2, PIANO2);
            Dati.salva(sp3, PIANO3);
            Dati.salva(so, OCCUPATE);
            
            System.out.println("Salvataggio dati completato.");
            
            System.exit(0);
        }
    }
    
    public Albergo(){
    
        this.setSize(300, 300);
        this.setTitle("Gestione Hotel v.1.0");
        this.setLayout(new FlowLayout());
        this.setBackground(Color.LIGHT_GRAY);
        
        this.piano1 = new LinkedList<Stanza>();
        this.piano2 = new LinkedList<Stanza>();
        this.piano3 = new LinkedList<Stanza>();
        this.occupate = new LinkedList<Stanza>();
        
        this.bCarica = new Button("Carica...");
        this.bCerca = new Button("Cerca...");
        this.bPrenota = new Button("Prenota");
        this.bSalva = new Button("Salva");
        
        this.add(bCarica);
        this.add(bCerca);
        this.add(bPrenota);
        this.add(bSalva);
        this.setVisible(true);
        
        this.addWindowListener(new Chiusura(piano1, piano2, piano3, occupate));
        
        bCarica.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
                Dati.carica(piano1, PIANO1);
            }
        });
        bCerca.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                String inputStanza = JOptionPane.showInputDialog("Stanza da trovare:", new Panel());
                int stanza = Integer.parseInt(inputStanza);
                
                boolean trovata = false;
                while(!trovata){
                trovata = Dati.ricerca(stanza, piano1);
                trovata = Dati.ricerca(stanza, piano2);
                trovata = Dati.ricerca(stanza, piano3);
                break;
                }
                String msg = (trovata) ? "Stanza " + inputStanza + "trovata." :
                        "Nessun risultato!"; 
                JOptionPane.showMessageDialog(null, msg, "Risultati ricerca", JOptionPane.INFORMATION_MESSAGE);
            }
            
            
        });
        bSalva.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                Dati.salva(new int[]{}, "");
            }
        });
        bPrenota.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                LinkedList<Stanza> pianoScelto = null;
                
                String inputPiano = JOptionPane.showInputDialog("Scegli piano", new Panel());
                String inputStanza = JOptionPane.showInputDialog("Scegli stanza", new Panel());
                
                int piano = Integer.parseInt(inputPiano);
                int stanza = Integer.parseInt(inputStanza);
                
                switch(piano){
                
                    case 1: 
                        pianoScelto = piano1;
                        break;
                
                     case 2: 
                        pianoScelto = piano2;
                        break;
                         
                     case 3:
                        pianoScelto = piano3;
                        break;
                     default:
                         
            }
                Dati.prenota(stanza, pianoScelto);
                JOptionPane.showMessageDialog(null, "Grazie!",
                        "Prenotazione riuscita!",
                        JOptionPane.INFORMATION_MESSAGE);
        }});
             
    }
        
  

    public LinkedList<Stanza> getPiano1() {
        return piano1;
    }

    public void setPiano1(LinkedList<Stanza> piano1) {
        this.piano1 = piano1;
    }

    public LinkedList<Stanza> getPiano2() {
        return piano2;
    }

    public void setPiano2(LinkedList<Stanza> piano2) {
        this.piano2 = piano2;
    }

    public LinkedList<Stanza> getPiano3() {
        return piano3;
    }

    public void setPiano3(LinkedList<Stanza> piano3) {
        this.piano3 = piano3;
    }

    public LinkedList<Stanza> getOccupate() {
        return occupate;
    }

    public void setOccupate(LinkedList<Stanza> occupate) {
        this.occupate = occupate;
    }

    
}

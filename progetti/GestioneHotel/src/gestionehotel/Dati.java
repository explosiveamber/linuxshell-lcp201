/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionehotel;

/**
 */
 /*
 * @author explosiveamber
 */

import java.util.LinkedList;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Dati {

    
    public static void salva(int[] numeri, String percorso){
    
        try {
            
            DataOutputStream dout = new DataOutputStream(new FileOutputStream(percorso));
            for(int num : numeri){
            
                dout.write(num);
            }
            dout.flush();
            dout.close();
        } catch (FileNotFoundException ex) {
           
            System.err.println("Impossibile trovare il file specificato.");
            
        } catch (IOException ex) {
             System.err.println("Impossibile trovare il file specificato.");
             
        }
    }
    
    public static Stanza[] carica(LinkedList<Stanza> piano, String percorso){
         // metodo modificato rispetto all'API originaria, non ritorna int[]
        try {
            DataInputStream din = new DataInputStream(new FileInputStream(percorso));
            while(din != null){ 
            
               Stanza s = new Stanza();
               s.setNumero(din.readInt());
               piano.add(s);
            }
            
        } catch (FileNotFoundException ex) {
            
            System.err.println("Impossibile trovare il file specificato.");
       
        } catch (IOException ex) {
            System.err.println("Formato dati errato.");
        }
        
        // Un cast piuttosto spinto
        return (Stanza[])piano.toArray();
        
    }
    
    public static boolean ricerca(int numero, LinkedList<Stanza> piano){
    
        for(Stanza s : piano){
        
            if(s.getNumero() == numero) break;
            return true;
        }
        return false;
    }
    
    public static void prenota(int numero, LinkedList<Stanza> piano){
    
        Stanza prenotata = new Stanza(numero);
        prenotata.setNumero(numero);
        piano.add(prenotata);
        
    }
}

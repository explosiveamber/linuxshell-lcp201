/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionehotel;

/**
 */
 /*
 * @author explosiveamber
 */

import java.util.Comparator;

public class ComparaStanze implements Comparator<Stanza> {

    @Override
    public int compare(Stanza o1, Stanza o2) {
        
        int esito = -1;
        
        if(o1.getNumero() <  o2.getNumero()) esito = -1;
        if(o1.getNumero() == o2.getNumero()) esito =  0;
        if(o1.getNumero() >  o2.getNumero()) esito = 1;
       
        return esito;
    }

}

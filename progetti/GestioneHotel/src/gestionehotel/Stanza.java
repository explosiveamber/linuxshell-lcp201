/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gestionehotel;

/**
 */
 /*
 * @author explosiveamber
 */
public class Stanza {
    
    private boolean bagno;
    private int mq;
    private int numero;
    
    public Stanza(boolean bagno, int mq, int numero){
    
        this.bagno = bagno;
        this.mq = mq;
        this.numero = numero;
    }

    public Stanza(int numero){
    
        this.numero = numero;
    }
    
    public Stanza(){}

    public boolean isBagno() {
        return bagno;
    }

    public void setBagno(boolean bagno) {
        this.bagno = bagno;
    }

    public int getMq() {
        return mq;
    }

    public void setMq(int mq) {
        this.mq = mq;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    
}

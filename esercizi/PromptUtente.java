/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp201;

import java.util.Scanner;

/**
 */
 /*
 * @author explosiveamber
 */
public class PromptUtente {
    
    private final static String segreto = "linux";
    
    public static void main(String[] args){
    
        // dobbiamo creare un oggetto dal formato adeguato per i nostri scopi
        StringBuffer controllo = new StringBuffer();
        
        // fatto ciò, ora bisogna catturare l'input
        System.out.println("iNSERIRE NOME UTENTE");
        Scanner cattura = new Scanner(System.in);
        
        // appendiamo l'input così recuperato al nostro oggetto StrringBuffer
        controllo.append(cattura.nextLine());
        
        // controlliamo con il metodo contentEquals della classe String
        boolean autenticato = PromptUtente.segreto.contentEquals(controllo);
        
        // emettendo un messaggio d'errore se risulta non autenticato
        if(!autenticato) System.out.println("NOME UTENTE ERRATO.\n RIPROVA");
   
    }

}

/*
 * Copyright (C) 2017 explosiveamber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package lcp201;

/**
 */
 /*
 * @author explosiveamber
 */
public class Relativita {
    
    public static double calcolaLunghezza(double k, double v, double c){
    
        /** Applica la teoria della relatività al calcolo di una lunghezza **/
        
        /* 
        
        Suggerimenti: questa formula è costituita da un gruppo di termini sotto
        radice o radicale e da un termine esterno o radicando.
        Per la soluzione conviene definire in variabili distinte
          a.il radicale
          b.il radicando
        In quest'ultimo caso data la semplicità della formula la variabile di 
        uscita coincide con quella usata per definire il radicando.
        Per i dettagli vedere Centro.java
        
        */
    
        double radicale = 1 - Math.pow((v / c), 2); // radicale
        
        double lunghezza = k * Math.sqrt(radicale); // radicando
        
        return lunghezza;
        
    }

}
